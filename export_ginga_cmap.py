#! /usr/bin/env python
#
# example4_mpl.py -- Load a fits file into a Ginga widget with a
#          matplotlib backend.
#
# This is open-source software licensed under a BSD license.
# Please see the file LICENSE.txt for details.
#
#

import os

import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

import numpy as np

from astropy.wcs import WCS
from astropy.io import fits

from ginga.mplw.ImageViewCanvasMpl import ImageViewCanvas
from ginga.misc import log
from ginga import cmap
from ginga.util.loader import load_data


class GingaMpl(object):
    """
    This class allows to do transformations interactively with Ginga in a
    matplotlib figure (see the Ginga Quick Reference
    http://ginga.readthedocs.io/en/latest/quickref.html) It opens a fits file,
    and a array of RGB colors of the current colormap can be exported.
    """

    # TODO: default params dict
    # default_params = {
    #     'image_path': 'example/example1.fits',
    #     'cmap': 'viridis',
    #     }

    gen_cmaps_path = 'generated_cmaps/'

    def __init__(self,
                 image_path='examples/color1.fits',
                 cmap_cb='viridis',
                 autocut_op='minmax',
                 set_cmap_filename=None,
                 **kwargs):

        self.image_path = image_path
        self.cmap_cb = cmap_cb
        self.autocut_op = autocut_op
        self.set_cmap_filename = set_cmap_filename

        self.logger = None
        self.image = None
        self.ax = None
        self.fi = None
        self.current_cmap = None
        self.cmap_rgbarray = None
        self.cmap_filename = None

        # add matplotlib colormaps to ginga's own set
        cmap.add_matplotlib_cmaps()

        self.load_image()

    def load_image(self):
        # Set to True to get diagnostic logging output
        use_logger = True
        self.logger = log.get_logger(null=not use_logger, log_stderr=True)
        self.image = load_data(self.image_path, logger=self.logger)

    def view(self):
        # just in case you want to use qt
        os.environ['QT_API'] = 'pyqt'

        # options = ['Qt4Agg', 'GTK', 'GTKAgg', 'MacOSX', 'GTKCairo', 'WXAgg',
        #           'TkAgg', 'QtAgg', 'FltkAgg', 'WX']

        fig = plt.figure()

        # create a ginga object, initialize some defaults and
        # tell it about the figure
        fi = ImageViewCanvas(logger=self.logger)
        fi.enable_autocuts('on')
        fi.set_autocut_params(self.autocut_op)
        fi.set_cmap(cmap.get_cmap(self.cmap_cb))
        fi.set_figure(fig)

        # enable all interactive ginga features
        fi.get_bindings().enable_all(True)

        fi.set_image(self.image)

        # Note adding axis from ginga (mpl backend) object
        self.ax = fi.add_axes()

        self.fi = fi

        plt.show(block=False)

    def get_cmap(self):
        self.rgbmap = self.fi.rgbmap
        rgb_len = len(self.rgbmap.sarr)
        self.cmap_rgbarray = np.array([self.rgbmap.get_rgbval(i)
                                       for i in range(rgb_len)]) / rgb_len

        self.current_cmap = ListedColormap(self.cmap_rgbarray)

    def export_cmap(self, print_array=True):
        self.get_cmap()
        ginga_cmap = self.rgbmap.cmap
        self.cmap_filename = self.set_cmap_filename \
            if self.set_cmap_filename is not None \
            else "{}{:.2f}.txt".format(ginga_cmap.name,
                                       self.rgbmap.scale_pct)

        np.savetxt(self.gen_cmaps_path+self.cmap_filename,
                   self.cmap_rgbarray)

        if print_array:
            print(repr(self.cmap_rgbarray))

    # TODO: add an interactive button to call self.export_cmap()

    @classmethod
    def load_cmap(self, cmap_filename, print_array=True):
        self.cmap_rgbarray = np.loadtxt(self.gen_cmaps_path+cmap_filename)
        self.current_cmap = ListedColormap(self.cmap_rgbarray)
        if print_array:
            print(repr(self.cmap_rgbarray))

    def plot(self, image_path=None, cmap_filename=None, naxis=2,
             remove_nans=True):
        cmap_filename = cmap_filename if cmap_filename is not None \
            else self.cmap_filename
        image_path = image_path if image_path is not None \
            else self.image_path

        self.load_cmap(cmap_filename, print_array=False)

        hdu = fits.open(image_path)[0]
        # hdr = hdu.header
        wcs = WCS(hdu.header).celestial

        if naxis == 0:
            image_data = hdu.data
        elif naxis == 1:
            image_data = hdu.data[0]
        elif naxis == 2:
            image_data = hdu.data[0][0]

        if remove_nans:
            image_data[np.isnan(image_data)] = 0

        fig2 = plt.figure(figsize=(8, 8))
        ax = plt.subplot(projection=wcs)

        im = ax.imshow(image_data,
                       cmap=self.current_cmap,
                       origin='lower')

        fig2.colorbar(im, ax=ax)
        plt.show(block=False)
