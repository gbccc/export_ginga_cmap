# export\_ginga_cmap


This simple script allows you to export the array of RGB colors of a colormap which has been manually stretched and scaled using Ginga control commands. 


### Example

Run the example using the python interpreter. First, import the GingaMpl class:

```python
>>> from export_ginga_cmap import GingaMpl
```

Alternatively:

```sh
$ sh export_ginga_cmap.sh
```

The latter opens an interpreter with some imported packages. Then:

```python
>>> viewer = GingaMpl(
    image_path="examples/color1.fits",
    cmap_cb="viridis")
>>> viewer.view()
```

Change the contrast, cmap, etc, using ginga control commands (see the [Ginga Quick Reference](https://ginga.readthedocs.io/en/latest/quickref.html#cmap-mode)). For example, press <kbd>space</kbd>, <kbd>y</kbd>, and <kbd>c</kbd>, to add a colorbar. Press <kbd>space</kbd>, <kbd>y</kbd>, and <kbd>up</kbd> or <kbd>down</kbd> buttons to choose a colormap. Press <kbd>space</kbd>, <kbd>t</kbd>, and control the contrast with the mouse first button. Once you are satisfied, export the colormap:

```python
>>> viewer.export_cmap()
```

The array with RGB colors will be printed and exported in generated_cmaps directory. The colormap can be used in a python interface with:


```python
>>> Viewer().load_cmap(cmap_filename="cmapname.txt")
```

Or:

```python
>>> from matplotlib.colors import ListedColormap
>>> import numpy as np
>>>
>>> cmap_array = np.loadtxt("/path/to/colorbar.txt")
>>> cmap = ListedColormap(cmap_array)
```

If you want to plot using matplotlib.pyplot.imshow:

```python
>>> viewer.plot()
```

### Useful colormaps


-  rainbow4: Same colormap as CASA







